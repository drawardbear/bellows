'''

Class to present the toolbar labels and connect them with thier functions.

'''

# -------------------------------------------------------------------
from PySide6.QtGui import QAction
from PySide6.QtWidgets import QToolBar
# -------------------------------------------------------------------


class Toolbar:
    # -------------------------------------------------------------------
    def __init__(self, toolbarButtons) -> None:
        self.toolbarActions = []
        for k, v in toolbarButtons.items():
            action = QAction(k)
            action.triggered.connect(v)
            self.toolbarActions.append(action)

        self.toolbar = QToolBar("Main toolbar")
        for action in self.toolbarActions:
            self.toolbar.addAction(action)
