#
# Sqlite database for storing task and asset data.
#

# -------------------------------------------------------------------
import os

from PySide6.QtSql import QSqlDatabase
from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker


# -------------------------------------------------------------------


class CupboardDb:
    """
    Class for creating, opening, providing sessions to the database.
    """
    base = declarative_base()

    # -------------------------------------------------------------------
    def __init__(self, file) -> None:
        self.db = None
        self.engine = None
        self.Session = None
        self.path = '/'.join(str.split(file, "/")[:-1]) + "/"
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        self.open(file)

    # -------------------------------------------------------------------
    def open(self, file):
        """
        Open a Sqlite database. If a database is open, close it. If a 
        database at file does not exist crate it.

        @param file str The file path to the database file.
        """
        if self.db is not None:
            CupboardDb.close()
        sql_str = 'sqlite:///' + file
        self.engine = create_engine(sql_str)

        if not os.path.exists(file):
            self.create_db()

        self.db = QSqlDatabase.addDatabase('QSQLITE')
        self.db.setDatabaseName(file)

        self.Session = sessionmaker(bind=self.engine)

    # -------------------------------------------------------------------
    @staticmethod
    def close():
        QSqlDatabase.removeDatabase('QSQLITE')

    # -------------------------------------------------------------------
    def create_db(self) -> None:
        CupboardDb.base.metadata.create_all(self.engine)

    # -------------------------------------------------------------------
    def get_session(self):
        session = self.Session()
        return session
