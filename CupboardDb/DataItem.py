#
# Assets table definition.
#

# -------------------------------------------------------------------
from sqlalchemy import Column, String

from CupboardDb.CupboardDb import CupboardDb


# -------------------------------------------------------------------


class DataItem(CupboardDb.base):
    __tablename__ = "data_item"

    location = Column(String, primary_key=True)
    fileType = Column(String)

    # -------------------------------------------------------------------
    def __repr__(self):
        return (f"location={self.location}, "
                f"fileType={self.fileType}, ")

    # -------------------------------------------------------------------
    def tags(self):
        return [self.fileType]

    # -------------------------------------------------------------------
    @staticmethod
    def add_item(session, location, file_type):
        instance = session.query(DataItem.location).filter(DataItem.location == location).first()
        if not instance:
            a = DataItem(location=location, fileType=file_type)
            session.add(a)
            session.commit()

    # -------------------------------------------------------------------
    @staticmethod
    def del_item(session, location):
        session.query(DataItem).filter(DataItem.location == location).delete()
        session.commit()

    # -------------------------------------------------------------------
    @staticmethod
    def len(session):
        return session.query(DataItem).count()

    # -------------------------------------------------------------------
    @staticmethod
    def all_tags(session):
        data = session.query(DataItem).all()
        tags = set()
        for d in data:
            tags.update(d.tags())
        return tags
