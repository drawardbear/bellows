#
# Assets table definition.
#

# -------------------------------------------------------------------
from sqlalchemy import Column, String

from CupboardDb.CupboardDb import CupboardDb


# -------------------------------------------------------------------


class TagItem(CupboardDb.base):
    __tablename__ = "filter_item"

    tag = Column(String, primary_key=True)
    tag_second = Column(String, primary_key=True)

    # -------------------------------------------------------------------
    def __repr__(self):
        return (f"<tag={self.tag}, "
                f"secondary tag={self.tag_second}>")

    # -------------------------------------------------------------------
    @staticmethod
    def all(session):
        data = session.query(TagItem).all()
        return data

    # -------------------------------------------------------------------
    @staticmethod
    def all_names(session):
        return [tag.tag for tag in TagItem.all(session)]

    # -------------------------------------------------------------------
    @staticmethod
    def add_tag(session, tag, tag_second=None):
        tag_second = tag_second if tag_second else ""
        a = TagItem(tag=tag, tag_second=tag_second)
        session.add(a)
        session.commit()

    # -------------------------------------------------------------------
    @staticmethod
    def del_tag(session, tag, tag_second=None):
        (session
         .query(TagItem)
         .filter(TagItem.tag == tag)
         .filter(TagItem.tag_second == tag_second)
         .delete())
        session.commit()
