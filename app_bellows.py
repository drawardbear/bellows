# -------------------------------------------------------------------
import configparser
import os
import shutil
import sys

from PySide6.QtWidgets import (QApplication, QMainWindow,
                               QVBoxLayout, QWidget)

from CupboardDb.CupboardDb import CupboardDb
from CupboardDb.DataItem import DataItem
from DataUI.dataUi import DataUi
from DataUI.utils import add_folder
from toolbar import Toolbar


class MainWindow(QMainWindow):
    configFile = 'params.ini'

    # -------------------------------------------------------------------
    def __init__(self):
        super().__init__()

        # Config file
        config = configparser.ConfigParser()
        config.read(MainWindow.configFile)
        self.dataPath = config['DEFAULT']['DATA_PATH']
        self.dbFileName = config['DEFAULT']['DB_FILE_NAME']
        self.houdiniLoc = config['DEFAULT']['HOUDINI_LOC']
        self.defaultHip = config['DEFAULT']['DEFAULT_HIP']
        self.config = config

        # Connect to DB and setup DB
        self.db = CupboardDb(self.dataPath + self.dbFileName)
        add_folder(self.db.get_session(), self.dataPath)

        # Setup UI
        self.dataUi = DataUi(self.db.get_session(), self.dataPath)
        self.dataUi.refresh_files()

        # Toolbar
        toolbar_buttons = {"New Hip": self.new_hip,
                           "Delete Hip": self.del_hip,
                           "Launch Houdini": self.launch_houdini}

        self.myToolBar = Toolbar(toolbar_buttons)
        self.addToolBar(self.myToolBar.toolbar)

        # Main Layout
        layout = QVBoxLayout()
        layout.addLayout(self.dataUi.layout)

        central_widget = QWidget()
        central_widget.setLayout(layout)

        self.setCentralWidget(central_widget)

    # -------------------------------------------------------------------
    # Toolbar Actions
    # -------------------------------------------------------------------
    def launch_houdini(self):
        file_name = self.dataUi.get_selected_location()

        terminal_cmd = f"open {self.houdiniLoc}"
        if file_name:
            terminal_cmd = terminal_cmd + f" {self.dataPath}{file_name}"

        os.system(terminal_cmd)

    # -------------------------------------------------------------------
    def new_hip(self):
        new_file = "new.hiplc"

        # Create hip file
        shutil.copyfile(self.defaultHip, self.dataPath + f"/{new_file}")

        # Add item to db and update view
        DataItem.add_item(self.db.get_session(), new_file, "hip")
        self.dataUi.refresh_files()

        # Select the newly created item
        num_rows = DataItem.len(self.db.get_session())
        index = self.dataUi.view.model().index(num_rows - 1, 0)
        self.dataUi.view.setCurrentIndex(index)

    # -------------------------------------------------------------------
    def del_hip(self):
        self.dataUi.del_selected()


# -------------------------------------------------------------------
app = QApplication(sys.argv)
window = MainWindow()
window.resize(900, 600)
window.show()

app.exec()
