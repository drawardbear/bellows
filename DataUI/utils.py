import os

from CupboardDb.DataItem import DataItem


def add_folder(session, folder):
    folder_items = os.listdir(folder)
    db_items = session.query(DataItem.location).all()
    db_items = [i[0] for i in db_items]

    items_to_add = [i for i in folder_items if i not in db_items]
    items_to_del = [i for i in db_items if i not in folder_items]

    for item in items_to_add:
        DataItem.add_item(session, item, item.split(".")[-1])

    for item in items_to_del:
        DataItem.del_item(session, item)
