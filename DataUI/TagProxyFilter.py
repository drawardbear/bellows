from PySide6.QtCore import QSortFilterProxyModel

from CupboardDb.TagItem import TagItem


class TagProxyFilter(QSortFilterProxyModel):
    def __init__(self, session):
        super(TagProxyFilter, self).__init__()
        self.session = session

    def filterAcceptsRow(self, source_row, source_parent):
        data = self.sourceModel().index(source_row, 1).data()
        visible_tags = TagItem.all_names(self.session)

        if not visible_tags or data in visible_tags:
            return True

        return False
