import os
import shutil

from PySide6.QtSql import QSqlTableModel
from PySide6.QtWidgets import (QAbstractItemView, QHBoxLayout, QVBoxLayout, QHeaderView,
                               QTableView, QComboBox)

from CupboardDb import DataItem, TagItem
from DataUI.FilterView import FilterView
from DataUI.TagProxyFilter import TagProxyFilter


class DataUi:

    def __init__(self, session, data_path) -> None:
        self.session = session
        self.dataPath = data_path
        self.selectedLocation = None

        # Model and view of files
        self.model = QSqlTableModel()
        self.model.setTable("data_item")
        self.model.setEditStrategy(QSqlTableModel.OnFieldChange)
        self.model.select()
        self.model.beforeUpdate.connect(
            lambda x, y: self.hip_naming_callback(x, y))

        # Proxy Filter Model of files
        self.view_filter = TagProxyFilter(self.session)
        self.view_filter.setSourceModel(self.model)

        # View of files
        self.view = QTableView()
        self.view.setModel(self.view_filter)
        self.view.setWindowTitle("Data Table")
        self.view.setSortingEnabled(True)
        self.view.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.view.setSelectionMode(QAbstractItemView.SingleSelection)
        self.view.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        self.view.selectionModel().selectionChanged.connect(
            lambda x, y: self.selection_changed_callback(x, y))

        # Add filter model and view
        self.filtermodel = QSqlTableModel()
        self.filtermodel.setTable("filter_item")
        self.filtermodel.select()
        self.filtermodel.setEditStrategy(QSqlTableModel.OnFieldChange)

        self.filterview = FilterView(self.filtermodel, "Filter Table")
        self.filterview.filters_modified.connect(self.filters_modified)

        # Add filter selection widget
        self.filter = QComboBox()
        self.filter.textActivated.connect(self.update_filters)
        self.set_combobox_items()

        # Setup page
        self.layout_filter = QVBoxLayout()
        self.layout_filter.addWidget(self.filter)
        self.layout_filter.addWidget(self.filterview)

        self.layout = QHBoxLayout()
        self.layout.addLayout(self.layout_filter)
        self.layout.addWidget(self.view)

    # -------------------------------------------------------------------
    def filters_modified(self):
        self.refresh_files()

    # -------------------------------------------------------------------
    def set_combobox_items(self):
        all_tags = DataItem.all_tags(self.session)
        used_tags = TagItem.all_names(self.session)
        all_unused_tags = [tag for tag in all_tags if tag not in used_tags]

        self.filter.clear()
        for i in all_unused_tags:
            self.filter.addItem(i)

    # -------------------------------------------------------------------
    def update_filters(self, txt):
        TagItem.add_tag(self.session, txt)
        self.refresh_filters()
        self.refresh_files()
        self.set_combobox_items()

    # -------------------------------------------------------------------
    def refresh_files(self):
        # Get selection before refreshing model with data
        selection = self.view.selectionModel().selectedIndexes()
        if selection:
            row = selection[0].row()
            col = selection[0].column()
            selected = self.view.model().index(row, col)

        # Refresh model with data in db
        self.model.select()

        # Set selection to what it was before data refresh_files
        if selection:
            self.view.setCurrentIndex(selected)

    # -------------------------------------------------------------------
    def refresh_filters(self):
        self.filtermodel.select()

    # -------------------------------------------------------------------
    def selection_changed_callback(self, selected, deselected):
        if selected.size():
            self.selectedLocation = [a.data()
                                     for a in selected.indexes()][1]

    # -------------------------------------------------------------------
    def hip_naming_callback(self, row, record):
        file_name = record.value("location")
        file_name = DataUi.hip_naming(file_name)

        shutil.move(
            self.dataPath + f"/{self.selectedLocation}", self.dataPath + f"/{file_name}")
        record.setValue("location", file_name)
        self.selectedLocation = file_name

    # -------------------------------------------------------------------
    def del_selected(self):
        os.remove(self.dataPath + f"/{self.selectedLocation}")
        row = self.view.selectedIndexes()[0].row()
        self.model.removeRow(row)
        self.refresh_files()

    # -------------------------------------------------------------------
    def get_selected_location(self):
        return self.selectedLocation

    # -------------------------------------------------------------------
    @staticmethod
    def hip_naming(file_name):
        hip_extension = ".hiplc"
        if len(file_name) < 7 or file_name[-len(hip_extension):] != hip_extension:
            return file_name + hip_extension

        return file_name
