from PySide6.QtCore import Qt, Signal
from PySide6.QtGui import QKeyEvent
from PySide6.QtWidgets import QAbstractItemView, QHeaderView
from PySide6.QtWidgets import QTableView


class FilterView(QTableView):
    filters_modified = Signal()

    def __init__(self, model, title):
        super(FilterView, self).__init__()
        self.setModel(model)
        self.setWindowTitle(title)
        self.setSortingEnabled(True)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)

    def keyPressEvent(self, event: QKeyEvent) -> None:
        if event.key() in [Qt.Key.Key_Backspace, Qt.Key.Key_Delete]:
            rows = set(i.row() for i in self.selectedIndexes())
            for row in rows:
                self.model().removeRow(row)
        self.model().select()
        self.filters_modified.emit()
